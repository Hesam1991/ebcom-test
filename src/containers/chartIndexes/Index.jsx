import React, { useEffect, useState } 				from "react";
import { indexesObj } 								from "../../utils/constant";

const ChartIndexes = ({ hourlyPairKeysHandler }) => {
	const [selectedIndexes, setSelectedIndexes] = useState([]);

	useEffect(() => {
		if (selectedIndexes?.length == 0) {
			hourlyPairKeysHandler(Object.keys(indexesObj));
		} else {
			hourlyPairKeysHandler(selectedIndexes);
		}
	}, [selectedIndexes]);

	const selectedIndexesChangeHandler = ({ target }) => {
		const checkoboxValue = target?.value;
		if (selectedIndexes?.includes(checkoboxValue)) {
			const newSelectedIndexes = selectedIndexes?.filter(
				(item) => item !== checkoboxValue
			);
			setSelectedIndexes(newSelectedIndexes);
		} else {
			setSelectedIndexes([...selectedIndexes, target.value]);
		}
	};
	return (
		<div className="ChartIndexesContainer card basis-100 p30">
			<h3>Indexes</h3>
			<section>
				<section className="flx-column">
					<div className="flx m10 align-items-center">
						<input
							className="display-none"
							type="checkbox"
							id="Higher"
							value="Higher"
							checked={selectedIndexes?.includes("Higher")}
							onChange={selectedIndexesChangeHandler}
						/>
						<div
							className={`pointer psudo-checkbox RCR-10 mr5 higher ${
								selectedIndexes?.includes("Higher") && "active"
							}`}
						></div>
						<label className="pointer" htmlFor="Higher">
							Higher
						</label>
					</div>
					<div className="flx m10 align-items-center">
						<input
							className="display-none"
							type="checkbox"
							id="Average"
							value="Average"
							checked={selectedIndexes?.includes("Average")}
							onChange={selectedIndexesChangeHandler}
						/>
						<div
							className={`pointer psudo-checkbox RCR-10 mr5 average ${
								selectedIndexes?.includes("Average") && "active"
							}`}
						></div>
						<label className="pointer" htmlFor="Average">
							Average
						</label>
					</div>
					<div className="flx m10 align-items-center">
						<input
							className="display-none"
							type="checkbox"
							id="Lower"
							value="Lower"
							checked={selectedIndexes?.includes("Lower")}
							onChange={selectedIndexesChangeHandler}
						/>
						<div
							className={`pointer psudo-checkbox RCR-10 mr5 lower ${
								selectedIndexes?.includes("Lower") && "active"
							}`}
						></div>
						<label className="pointer" htmlFor="Lower">
							Lower
						</label>
					</div>
				</section>
			</section>
		</div>
	);
};

export default ChartIndexes;
