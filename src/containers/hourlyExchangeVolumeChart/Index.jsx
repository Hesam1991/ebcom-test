import React              from "react";
import { ResponsiveBar }  from "@nivo/bar";
import { barColors }      from "../../utils/constant";
import                         "slick-carousel/slick/slick.css";
import                         "slick-carousel/slick/slick-theme.css";
import Slider              from "react-slick";

const HourlyExchangeVolume = ({ chartData }) => {
	var settings = {
		dots: true,
		infinite: true,
		speed: 500,
		autoplaySpeed: 1800,
		centerMode: true,
		centerPadding: 0,
		adaptiveHeight: true,
		autoplay: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		className: "test",
	};

	const charts = chartData?.map((hourlyData, index) => {
		const date = new Date(hourlyData?.time * 1000);
		const data = [
			{
				...hourlyData,
				Time: `${date?.getHours()}:${date?.getMinutes()}`,
				Val: hourlyData?.volume,
				ValColor: barColors?.higher,
			},
		];

		return (
			<div key={`element_${index}`}>
				<div style={{ padding: "5px", width: "100%" }}>
					<div style={{ width: "100%", height: "400px" }}>
						<ResponsiveBar
							data={data ? data : []}
							keys={["volume"]}
							indexBy="Time"
							margin={{ top: 50, right: 50, bottom: 40, left: 100 }}
							padding={0.8}
							innerPadding={2}
							groupMode="grouped"
							valueScale={{ type: "linear" }}
							indexScale={{ type: "band", round: true }}
							colors={barColors?.higher}
							borderRadius={3}
							borderColor={{ from: "color", modifiers: [["darker", "1.4"]] }}
							enableGridY={false}
							enableGridX={false}
							axisBottom={null}
							axisTop={{
								tickSize: 5,
								tickPadding: 10,
								tickRotation: 0,
								legend: `Market volume of ${data?.[0]?.Time}`,
								legendPosition: "middle",
								legendOffset: -40,
							}}
							axisLeft={{
								tickSize: 5,
								tickPadding: 5,
								tickRotation: 0,
								legend: "",
								legendPosition: "middle",
								legendOffset: -40,
								// tickValues: [10000, 20000, 30000],
							}}
							legends={[]}
							isInteractive={false}
							enableLabel={false}
							animate={true}
							motionStiffness={90}
							motionDamping={15}
						/>
					</div>
				</div>
			</div>
		);
	});

	return (
		<section className="HourlyExchangeVolumeContainer basis-30 m5 card">
			<Slider {...settings}>{charts}</Slider>
		</section>
	);
};

export default HourlyExchangeVolume;
