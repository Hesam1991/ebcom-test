import Index from "./containers/Index";
import            "./assets/styles/app.css";

function App() {
  return <Index />;
}

export default App;
