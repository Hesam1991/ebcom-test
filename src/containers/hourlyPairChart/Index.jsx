import React, { useEffect, useState } 		from "react";
import { ResponsiveBar } 					from "@nivo/bar";
import { barColors, indexesObj } 			from "../../utils/constant";

const HourlyPair = ({ chartData, keys }) => {
	const [chartColors, setChartColors] = useState(Object.values(barColors));

	useEffect(() => {
		const colors = [];
		Object.keys(indexesObj)?.map((each) => {
			if (keys?.includes(each)) colors.push(indexesObj?.[each]?.color);
		});
		setChartColors(colors);
	}, [keys]);
	const data = chartData?.Data?.map((eachRecord) => {
		const Average = (eachRecord?.high + eachRecord?.low) / 2;
		const date = new Date(eachRecord?.time * 1000);

		return {
			...eachRecord,
			Higher: eachRecord?.high,
			// HigherColor: barColors?.higher,
			Average,
			// AverageColor: barColors?.average,
			Lower: eachRecord?.low,
			// LowerColor: barColors?.lower,
			Time: `${date?.getHours()}:${date?.getMinutes()}`,
		};
	});

	return (
		<section className="HourlyPairContainer basis-70 m5 card ">
			<ResponsiveBar
				data={data ? data : []}
				keys={[...keys]}
				indexBy="Time"
				margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
				padding={0.4}
				innerPadding={2}
				groupMode="grouped"
				valueScale={{ type: "linear" }}
				indexScale={{ type: "band", round: true }}
				colors={chartColors}
				borderRadius={3}
				borderColor={{ from: "color", modifiers: [["darker", "1.4"]] }}
				axisTop={null}
				axisRight={null}
				enableGridY={false}
				enableGridX={false}
				axisBottom={{
					tickSize: 5,
					tickPadding: 5,
					tickRotation: 0,
					legend: "",
					legendPosition: "middle",
					legendOffset: 32,
				}}
				axisLeft={{
					tickSize: 5,
					tickPadding: 5,
					tickRotation: 0,
					legend: "",
					legendPosition: "middle",
					legendOffset: -40,
					tickValues: [10000, 20000, 30000],
				}}
				legends={[]}
				isInteractive={false}
				enableLabel={false}
				animate={true}
				motionStiffness={90}
				motionDamping={15}
			/>
		</section>
	);
};

export default HourlyPair;
