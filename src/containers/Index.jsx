import React, { useEffect, useState }   from "react";
import HourlyPair                       from "./hourlyPairChart/Index";
import HourlyExchangeVolume             from "./hourlyExchangeVolumeChart/Index";
import ChartIndexes                     from "./chartIndexes/Index";
import { 
  getHourlyPairData,
  getHourlyExchangeVolumeData
}                                       from "./api";

const CryptoReports = () => {

  const [hourlyPairData, setHourlyPairData] = useState();
  const [volumeExData, setVolumeExData] = useState();
  const [hourlyPairKeys, setHourlyPairKeys] = useState(["Higher", "Average", "Lower"]);

  useEffect(() => {
    (async () => {
      try {
        const { data } = await getHourlyPairData({ fsym: "BTC", tsym: "USD", limit: 10 });
        setHourlyPairData(data);
      } catch (err) {
        console.error("An error occurred while getting Hourly Pair OHLCV Data", err);
      }
      try {
        const { data } = await getHourlyExchangeVolumeData({
          fsym: "BTC",
          tsym: "USD",
          limit: 10,
        });
        setVolumeExData(data);
      } catch (err) {
        console.error("An error occurred while getting Hourly Exchange Volume Data", err);
      }
    })();
  }, []);
  
  const hourlyPairChartKeysSetter = (keys) => {

    // Ordering keys for their colors
      const orderedKeysArray = [];
    ["Higher", "Average", "Lower"].map(eachKey => {
      if (keys?.includes(eachKey)) {
        orderedKeysArray?.push(eachKey);
      }
    })
    setHourlyPairKeys(orderedKeysArray) 
  }

  return (
    <main className="container">
      <section className="flx">
        <HourlyPair chartData={hourlyPairData?.Data} keys={hourlyPairKeys} />
        <HourlyExchangeVolume chartData={volumeExData?.Data}  />
      </section>
      <section className="p5">
        <ChartIndexes hourlyPairKeysHandler={hourlyPairChartKeysSetter}/>
      </section>
    </main>
  );
};

export default CryptoReports;
