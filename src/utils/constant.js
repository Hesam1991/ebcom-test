export const barColors = {
	higher: "#11b89b",
	average: "#ffd966",
	lower: "#f24b4b",
};

export const indexesObj = {
	Higher: {
		order: 1,
		color: barColors?.higher
	},
	Average: {
		order: 2,
		color: barColors?.average
	},
	Lower: {
		order: 3,
		color: barColors?.lower
	},
}