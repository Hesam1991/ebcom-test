import Request                                          from "../utils/Request";
import { hourlyExchangeVolPath, hourlyPairOhlcvPath }   from "../utils/apiRoutes";

export const getHourlyPairData = (params = {}) => Request.get(hourlyPairOhlcvPath, params);
export const getHourlyExchangeVolumeData = (params = {}) =>
  Request.get(hourlyExchangeVolPath, params);
