import axios from "axios";

class Request {
  static get = (url, params = {}, headers = {}, otherConfig = {}) => {
    const configObj = {
      params,
      headers,
      ...otherConfig,
    };
    const response = axios
      .get(url, configObj)
      .then((res) => res)
      .catch((err) => err);
    return response;
  };
}
export default Request;
