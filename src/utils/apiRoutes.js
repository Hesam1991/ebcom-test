const BASE_URL = "https://min-api.cryptocompare.com";

export const hourlyExchangeVolPath      = `${BASE_URL}/data/exchange/histohour`;
export const hourlyPairOhlcvPath        = `${BASE_URL}/data/v2/histohour`;
